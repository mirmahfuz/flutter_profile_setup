import 'package:flutter/material.dart';
import 'package:flutter_profile_details_page/profile_page_setup/user_profile_setup_models.dart';
import 'package:flutter_profile_details_page/user_profile_page_ui/profile_page_ui.dart';

class UserProfilePageUI extends StatelessWidget {

  static final User mir = User(
      backdropPhoto: 'assets/backdrop.png',
      avatar: 'assets/avatar.png',
      addPhoto: 'Add Photo',
      name: 'Nazirul Hoque',
      phoneNumber: '01712345678',
      status: 'This is Andy');

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: ProfilePageUI(mir),
    );
  }
}
