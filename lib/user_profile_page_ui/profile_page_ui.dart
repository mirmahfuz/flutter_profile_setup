import 'package:flutter/material.dart';
import 'package:flutter_profile_details_page/profile_page_setup/topBackgroundImage.dart';
import 'package:flutter_profile_details_page/profile_page_setup/user_profile_setup_models.dart';

class ProfilePageUI extends StatelessWidget {
  final User user;

  ProfilePageUI(this.user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: new Stack(
          children: <Widget>[
            TopBackgroundImage(user.backdropPhoto),
            Positioned(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _buildContent(),
              ],
            ))
          ],
        ),
      ),
    );
  }

  Widget _buildContent() {
    return Padding(
      padding: const EdgeInsets.only(top: 50.0),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            _buildAvatar(),
            _buildAddPhoto(),
            _buildUserFeature(),
          ],
        ),
      ),
    );
  }

  Widget _buildAvatar() {
    return Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(color: Colors.white),
      ),
      padding: const EdgeInsets.all(3.0),
      child: ClipOval(
        child: Image.asset(user.avatar),
      ),
    );
  }

  Widget _buildAddPhoto() {
    return InkWell(
      onTap: (){
        print('Add Photo Tapped');
      },
      child: Padding(
          padding: const EdgeInsets.only(
            top: 15.0,
          ),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20.0)),
              shape: BoxShape.rectangle,
              border: Border.all(color: Colors.white),
            ),
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 6.0, horizontal: 10.0),
              child: Text(
                user.addPhoto,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          )),
    );
  }

  Widget _buildUserFeature() {
    /// Name and Status Part

    return Column(
      children: <Widget>[
        new SizedBox(
          height: 20.0,
        ),
        new ListTile(
          leading: Icon(Icons.account_circle,color: Colors.black,),
          subtitle: new Text(user.name,style: TextStyle(fontSize: 15.0,color: Colors.black),),
          title: new Text('Name',style: TextStyle(fontSize: 12.0,color: Colors.black87),),
        ),
        new ListTile(
          leading: Icon(Icons.flag,color: Colors.black,),
          subtitle: new Text('Be Honest,Stay Honest',style: TextStyle(fontSize: 15.0,color: Colors.black),),
          title: new Text('Status',style: TextStyle(fontSize: 12.0,color: Colors.black87),),
        ),
        new ListTile(
          leading: Icon(Icons.phone_android,color: Colors.black,),
          subtitle: new Text(user.phoneNumber,style: TextStyle(fontSize: 15.0,color: Colors.black),),
          title: new Text('Change Number',style: TextStyle(fontSize: 12.0,color: Colors.black87),),
        ), new ListTile(
          leading: Icon(Icons.block,color: Colors.black,),
          title: new Text('Blocked Contacts'),
          ),
        new ListTile(
          leading: Icon(Icons.delete,color: Colors.black,),
          title: new Text('Delete Account'),),
        new ListTile(
          leading: Icon(Icons.input,color: Colors.black,),
          title: new Text('Logout'),
         ),
      ],
    );
  }
}
