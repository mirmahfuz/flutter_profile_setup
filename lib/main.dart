import 'package:flutter/material.dart';
import 'package:flutter_profile_details_page/profile_page_setup/user_profile_setup_models.dart';
import 'package:flutter_profile_details_page/profile_page_setup/user_profile_setup_page.dart';
import 'package:flutter_profile_details_page/user_profile_page_ui/profile_page_ui.dart';
import 'package:flutter_profile_details_page/user_profile_page_ui/user_profile_page_ui.dart';

void main() => runApp(UserProfilePageUI());

class MyApp extends StatelessWidget {

  static final User mir = User(
      backdropPhoto: 'assets/backdrop.png',
      avatar: 'assets/avatar.png',
      addPhoto: 'Add Photo',
      name: 'Andy',
      phoneNumber: '01712345678',
      status: 'This is Andy');

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: ProfileSetupPage(mir),

    );
  }
}
