import 'package:meta/meta.dart';

class User {

  final String backdropPhoto;
  final String avatar;
  final String addPhoto;
  final String name;
  final String status;
  final String phoneNumber;
  User({

   @required  this.backdropPhoto,
    @required this.avatar,
    @required this.addPhoto,
    @required this.name,
    @required this.status,
    this.phoneNumber,

});
}