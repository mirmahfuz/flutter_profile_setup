import 'package:flutter/material.dart';

class TopBackgroundImage extends StatelessWidget {
  TopBackgroundImage(this.backgroundImage);

  final String backgroundImage;

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;

    return Stack(
      children: <Widget>[
        Container(
          child: ClipPath(
           // clipper: ArcClipper(),
            child: Image.asset(
              backgroundImage,
              width: screenWidth,
              height: 220.0,
              fit: BoxFit.cover,
            ),
          ),
        ),
        Container(
          height: 220.0,
          child: ClipPath(
              //clipper: ArcClipper(),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    gradient: LinearGradient(
                        begin: FractionalOffset.bottomCenter,
                        end: FractionalOffset.topCenter,
                        colors: [
                          const Color(0xFF898dfb),
                          const Color(0x809968f8),
                        ],
                        stops: [
                          0.0,
                          1.0
                        ])),
              )),
        ),
      ],
    );
  }
}

class ArcClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0.0, size.height - 20);

    var firstControlPoint = Offset(size.width / 4, size.height);
    var firstPoint = Offset(size.width / 2, size.height);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstPoint.dx, firstPoint.dy);

    var secondControlPoint = Offset(size.width - (size.width / 4), size.height);
    var secondPoint = Offset(size.width, size.height - 20);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondPoint.dx, secondPoint.dy);

    path.lineTo(size.width, 0.0);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
