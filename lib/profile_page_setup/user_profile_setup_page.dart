import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:flutter_profile_details_page/profile_page_setup/topBackgroundImage.dart';
import 'package:flutter_profile_details_page/profile_page_setup/user_profile_setup_models.dart';

class ProfileSetupPage extends StatelessWidget {
  ProfileSetupPage(this.user);

  final User user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        //fit: StackFit.expand,
       child: new Stack(
         children: <Widget>[
           Padding(
             padding: EdgeInsets.only(bottom: 50.0),
             child: TopBackgroundImage(user.backdropPhoto),
           ),
           Positioned(
               child: Column(
                 crossAxisAlignment: CrossAxisAlignment.center,
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: <Widget>[
                   _buildContent(),
                 ],
               ))
         ],
       )
      ),
      bottomNavigationBar: new Container(
        //Finish button added
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _buildButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildContent() {
    return Padding(
      padding: const EdgeInsets.only(top:50.0),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            _buildAvatar(),
            _buildAddPhoto(),
            _buildUserInfo(),
          ],
        ),
      ),
    );
  }

  Widget _buildAvatar() {
    return Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(color: Colors.white),
      ),
      padding: const EdgeInsets.all(3.0),
      child: ClipOval(
        child: Image.asset(user.avatar),
      ),
    );
  }

  Widget _buildAddPhoto() {
    return Padding(
        padding: const EdgeInsets.only(
          top: 15.0,
        ),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            shape: BoxShape.rectangle,
            border: Border.all(color: Colors.white),
          ),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 6.0, horizontal: 10.0),
            child: Text(
              user.addPhoto,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ));
  }

  Widget _buildUserInfo() {

    /// Name and Status Part

    return Padding(
        padding: const EdgeInsets.only(
          left: 30.0,right: 30.0,top: 50.0,
        ),
        child: Column(
          children: <Widget>[
            TextField(
              style: TextStyle(
                  fontFamily: "WorkSansSemiBold",
                  fontSize: 16.0,
                  color: Colors.black12),
              decoration: InputDecoration(

                //border: InputBorder.none,
                suffixIcon: Icon(
                  Icons.account_circle,
                  color: Colors.black12,
                ),
                prefixIcon: Icon(
                  Icons.account_circle,
                  color: Colors.black12,

                ),
                hintText: "Enter your name",
                hintStyle:
                    TextStyle(fontFamily: "WorkSansSemiBold", fontSize: 17.0),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15.0),
              child: TextField(
                keyboardType: TextInputType.text,
                style: TextStyle(
                    fontFamily: "WorkSansSemiBold",
                    fontSize: 16.0,
                    color: Colors.black12),
                decoration: InputDecoration(
                  suffixIcon: Icon(
                    Icons.account_circle,
                    color: Colors.black12,

                  ),
                  //border: InputBorder.none,
                  prefixIcon: Icon(
                    Icons.account_circle,
                    color: Colors.black12,

                  ),
                  hintText: "Your Status",
                  hintStyle:
                      TextStyle(fontFamily: "WorkSansSemiBold", fontSize: 17.0),
                ),
              ),
            ),
          ],
        ));
  }

  Widget _buildButton() {
    //Finish Button
    return new Container(
      margin: EdgeInsets.only(top: 100.0),
      decoration: new BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(30.0)),
        boxShadow: <BoxShadow>[

          BoxShadow(
            color: const Color(0x609968f8),
            offset: Offset(1.0, 6.0),
            blurRadius: 5.0,
          ),
        ],
        gradient: new LinearGradient(
            colors: [
              const Color(0xFF898dfb),
              const Color(0x809968f8),
            ],
            begin: const FractionalOffset(0.2, 0.2),
            end: const FractionalOffset(1.0, 1.0),
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp),
      ),
      child: MaterialButton(
          highlightColor: Colors.transparent,
          splashColor: const Color(0xFF9968f8),
          //shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 20.0, horizontal: 100.0),
            child: Text(
              "Finish",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontFamily: "WorkSansNormal"),
            ),
          ),
          onPressed: () => print("Login button pressed")),
    );
  }
}
